package com.course.bestpricebookstore.bestpricebookstore.service;

import com.course.bestpricebookstore.bestpricebookstore.dtos.BestPriceBook;

public interface BookRetrievalService {

    BestPriceBook getBestPriceBook(String bookName) throws InterruptedException;
    

}

package com.course.bestpricebookstore.bestpricebookstore.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.StructuredTaskScope;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import com.course.bestpricebookstore.bestpricebookstore.dtos.BestPriceBook;
import com.course.bestpricebookstore.bestpricebookstore.dtos.Book;
import com.course.bestpricebookstore.bestpricebookstore.service.BookRetrievalService;

@Service
public class BookRetrievalServiceImpl implements BookRetrievalService {

    @Value("#{${book.store.baseurls}}")
    private Map<String, String> storeUrlMap;
    private RestClient client = RestClient.create();

    @Override
    public BestPriceBook getBestPriceBook(String bookName) {
        // TODO Auto-generated method stub
        final var bookList = this.getBookFromAllStores(bookName);
        throw new UnsupportedOperationException("Unimplemented method 'getBestPriceBook'");
    }

    private List<Book> getBookFromAllStores(String bookName) {
        try (var scope = new StructuredTaskScope<Book>()) {
            var bookTasks = new ArrayList<>();
            storeUrlMap.forEach((name, url) -> {
                bookTasks.add(scope.fork(() -> this.getBookFromStore(bookName)));
            });
            scope.join();

        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;
    }

    private Book getBookFromStore(String bookName) {
        return null;
    }

}

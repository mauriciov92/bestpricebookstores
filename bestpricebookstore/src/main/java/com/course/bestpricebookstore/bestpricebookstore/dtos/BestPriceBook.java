package com.course.bestpricebookstore.bestpricebookstore.dtos;
import java.util.List;


public record BestPriceBook(Book bestPriceDeal, List<Book> allDeals) {

}

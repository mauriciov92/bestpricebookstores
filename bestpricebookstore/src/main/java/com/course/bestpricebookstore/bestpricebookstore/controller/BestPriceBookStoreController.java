package com.course.bestpricebookstore.bestpricebookstore.controller;

import org.springframework.web.bind.annotation.RestController;

import com.course.bestpricebookstore.bestpricebookstore.dtos.BestPriceBook;
import com.course.bestpricebookstore.bestpricebookstore.service.BookRetrievalService;

import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/virtualstore")
@RequiredArgsConstructor
public class BestPriceBookStoreController {

    private final BookRetrievalService bookRetrievalService;

    @GetMapping("/book")
    public ResponseEntity<BestPriceBook> getBestPriceBook(@RequestParam String bookName) throws InterruptedException {
        final var bodyResponse = this.bookRetrievalService.getBestPriceBook(bookName);
        return ResponseEntity.ok(bodyResponse);
    }

}

package com.course.bookstore.bookstore.controller;

import org.springframework.web.bind.annotation.RestController;

import com.course.bookstore.bookstore.model.Book;
import com.course.bookstore.bookstore.service.BookCollection;

import lombok.RequiredArgsConstructor;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/store")
public class BookStoreController {
    
    @Autowired
    private BookCollection bookCollection;

    @GetMapping("/book")
    public Book getBookByName(@RequestParam String name) {
        
    	// Add an artificial delay of 5 secs for testing
        delayOf5Secs();
        
        return bookCollection.findBook(name);
    }

    private void delayOf5Secs() {
        try {
            Thread.sleep(Duration.ofSeconds(5));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

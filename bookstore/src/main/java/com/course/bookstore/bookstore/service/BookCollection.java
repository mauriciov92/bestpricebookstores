package com.course.bookstore.bookstore.service;

import com.course.bookstore.bookstore.model.Book;

public interface BookCollection {
    
    Book findBook(String name);
    
}
